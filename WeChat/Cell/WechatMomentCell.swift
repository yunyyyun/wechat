//
//  WechatMomentCell.swift
//  WeChat
//
//  Created by mengyun on 2019/11/15.
//  Copyright © 2019 mengyun. All rights reserved.
//

import UIKit

class WechatMomentCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource {

    @IBOutlet var avatarImage: UIImageView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var contentLabel: UILabel!
    
    @IBOutlet var imagesCollectionView: UICollectionView!
    @IBOutlet var imagesCollectionViewHeight: NSLayoutConstraint!
    
    @IBOutlet var commentsView: UIView!
    @IBOutlet var commentsViewHeight: NSLayoutConstraint!
    
    var data: WechatMomentCellData = WechatMomentCellData() {
        didSet{
            nameLabel.text = data.sender?.username
            contentLabel.text = data.content
            avatarImage.setImageWithURL(imageUrl: data.sender?.avatar, usePlaceholder: true)
            reloadImages()
            reloadComments()
            self.layoutIfNeeded()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        avatarImage.layer.cornerRadius = 4
        avatarImage.layer.masksToBounds = true
        
        let w = UIScreen.main.bounds.size.width-90
        let layout = UICollectionViewFlowLayout()
        let edge = CGFloat(3)
        layout.itemSize = CGSize(width: (w-4*edge)/3, height: (w-4*edge)/3)
        layout.sectionInset = UIEdgeInsets(top: edge, left: edge, bottom: edge, right: edge)
        layout.minimumLineSpacing = 3
        layout.minimumInteritemSpacing = 3
        // layout.scrollDirection = .vertical
        //设置collectionView的代理
        imagesCollectionView.collectionViewLayout = layout
        imagesCollectionView.autoresizingMask = [UIView.AutoresizingMask.flexibleHeight,UIView.AutoresizingMask.flexibleWidth]
        
        imagesCollectionView.isScrollEnabled = false
        imagesCollectionView.delegate = self
        imagesCollectionView.dataSource = self
        imagesCollectionView!.register(UINib(nibName:"ImageCollectionViewCell", bundle:nil),
                                      forCellWithReuseIdentifier: ImageCollectionViewCell.reuseId())
        
        self.selectionStyle = .none
        // imagesCollectionView.backgroundColor = .blue
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func reloadImages() {
        imagesCollectionView.reloadData()
        imagesCollectionViewHeight.constant = imagesCollectionView.collectionViewLayout.collectionViewContentSize.height
    }
    
    func reloadComments() {
        for v in commentsView.subviews{
            v.removeFromSuperview()
        }
        
        var y: CGFloat = 0
        if (data.comments != nil){
            for commemt in data.comments! {
                if (y<4){
                    y = y + 4
                }
                let font = UIFont.systemFont(ofSize: 14)
                let nick = ((commemt.sender?.nick) ?? "")
                let content = commemt.content
                let commentContent = nick + ": " + content
                let attributeCommentContent = NSMutableAttributedString(string: commentContent)
                attributeCommentContent.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor(red: 100/255, green: 110/255, blue: 150/255, alpha: 0.9), range: NSRange(location: 0, length: nick.count  + 1))
                attributeCommentContent.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor(white: 0.3, alpha: 0.9), range: NSRange(location: nick.count  + 2, length: content.count))
                
                let commemtHeight = heightWithWidth(width: commentsView.frame.size.width-10, text: commentContent, font: font)
                let commemtLabel = UILabel(frame: CGRect(x: 5, y: y, width: commentsView.frame.size.width-10, height: commemtHeight))
                y = y + commemtHeight
                y = y + 4
                commemtLabel.font = font
                commemtLabel.attributedText = attributeCommentContent
                commemtLabel.numberOfLines = 0

                commentsView.addSubview(commemtLabel)
            }
        }
        
        commentsViewHeight.constant = y
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.images?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell  = collectionView.dequeueReusableCell(withReuseIdentifier: ImageCollectionViewCell.reuseId(),
                                                       for: indexPath) as! ImageCollectionViewCell
        cell.contentImageView.setImageWithURL(imageUrl: data.images?[indexPath.item].url, usePlaceholder: true)
        return cell
    }
    
    class func reuseId() -> String {
        return self.description();
    }
    
//    class func heightForData(data: WechatMomentCellData) -> CGFloat{
//        let imageCount: CGFloat = CGFloat(data.images?.count ?? 0);
//        let height = 80 + heightWithWidth(width: 300, text: data.content, font: UIFont.systemFont(ofSize: 17))
//        if (imageCount<=0){
//            return height
//        }
//        return height + ((imageCount-1)/3+1)*(UIScreen.main.bounds.size.width/3-30) + 44
//    }
    
}

var label: UILabel?
func heightWithWidth(width: CGFloat,text: String, font: UIFont) -> CGFloat {
    if (label == nil){
        label = UILabel()
        label?.numberOfLines = 0;
    }
    label?.font = font
    label?.text = text
    let height = label?.sizeThatFits(CGSize(width: width, height: CGFloat.greatestFiniteMagnitude)).height ?? 0
    return ceil(height)
}
