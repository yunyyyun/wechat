//
//  ImageCollectionViewCell.swift
//  WeChat
//
//  Created by mengyun on 2019/11/16.
//  Copyright © 2019 mengyun. All rights reserved.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var contentImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        contentImageView.contentMode = .scaleToFill
    }
    
    class func reuseId() -> String {
        return self.description();
    }

}
