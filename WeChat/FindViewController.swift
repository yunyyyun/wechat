//
//  FindViewController.swift
//  WeChat
//
//  Created by mengyun on 2019/11/17.
//  Copyright © 2019 mengyun. All rights reserved.
//

import UIKit

class FindViewController: UIViewController {

    @IBOutlet var itemButton: UIButton!
    var sender: Sender?
    
    @IBOutlet var avatarImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = "发现"
        itemButton.addTarget(self, action: #selector(goToWechatMomentView), for: .touchUpInside)
        itemButton.layer.borderWidth = 0.5
        itemButton.layer.borderColor = UIColor.lightGray.cgColor
        
        Sender.getData { (sender) in
            
        }
    }


    @objc func goToWechatMomentView(){
        let vc = WechatMomentViewController()
        vc.sender = sender
        self.navigationController?.pushViewController(vc, animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
