//
//  WechatMomentCellData.swift
//  WeChat
//
//  Created by mengyun on 2019/11/15.
//  Copyright © 2019 mengyun. All rights reserved.
//

import UIKit
import HandyJSON

// 发布人
class Sender: HandyJSON {
    var profileImage: String = ""
    var username: String = ""
    var nick: String = ""
    var avatar: String = ""
    required init() {}
    
    func mapping(mapper: HelpingMapper) {
        mapper <<<
            self.profileImage <-- "profile-image"
    }
    
    class func getData(completionHandler: @escaping (_ data: Sender) -> Void){
        let url: URL = URL(string: "https://thoughtworks-mobile-2018.herokuapp.com/user/jsmith")!
        
        let cacheConfig = CacheConfig.shared
        // 从缓存中取
        var data: Data? = cacheConfig.getDataFromMemory(forKey: cacheSenderKey) as? Data
        if let goodData = data {
            guard let jsonString = String(data: goodData, encoding: String.Encoding.utf8) else {
                return
            }
            if let sender = Sender.deserialize(from: jsonString) {
                completionHandler(sender)
            }
        }
        
        // 从磁盘取
        data = cacheConfig.getDataFromDisk(forKey: cacheSenderKey) as? Data
        if let goodData = data {
            guard let jsonString = String(data: goodData, encoding: String.Encoding.utf8) else {
                return
            }
            if let sender = Sender.deserialize(from: jsonString) {
                cacheConfig.cacheDataToMemory(data as AnyObject, forKey: cacheSenderKey)
                completionHandler(sender)
            }
            return
        }
        
        let dataTask = URLSession.shared.dataTask(with: url) {(data, response, error) ->Void in
            if error != nil {
            }else{
                guard let jsonString = String(data: data!, encoding: String.Encoding.utf8) else {
                    return
                }
                if let sender = Sender.deserialize(from: jsonString) {
                    cacheConfig.cacheDataToMemory(data as AnyObject, forKey: cacheSenderKey)
                    cacheConfig.cacheDataToDisk(data as AnyObject, forKey: cacheSenderKey)
                    completionHandler(sender)
                }
            }
        }
        dataTask.resume()
    }
}

// 评论
class Comment: HandyJSON {
    var content: String = ""
    var sender: Sender?
    required init() {}
}

// 图片
class Image: HandyJSON {
    var url: String = ""
    required init() {}
}

class WechatMomentCellData: HandyJSON {
    var content: String = ""
    var images: [Image]?
    var sender: Sender?
    var comments: [Comment]?
    
    required init() {}
    
    class func getDatas(completionHandler: @escaping (_ data: [WechatMomentCellData]) -> Void){
        let url: URL = URL(string: "https://thoughtworks-mobile-2018.herokuapp.com/user/jsmith/tweets")!
        
        let cacheConfig = CacheConfig.shared
        // 从缓存中取
        var data: Data? = cacheConfig.getDataFromMemory(forKey: cacheListKey) as? Data
        if let goodData = data {
            guard let jsonString = String(data: goodData, encoding: String.Encoding.utf8) else {
                return
            }
            if let datas = [WechatMomentCellData].deserialize(from: jsonString) {
                completionHandler(datas as! [WechatMomentCellData])
            }
        }
        
        // 从磁盘取
        data = cacheConfig.getDataFromDisk(forKey: cacheListKey) as? Data
        if let goodData = data {
            guard let jsonString = String(data: goodData, encoding: String.Encoding.utf8) else {
                return
            }
            if let datas = [WechatMomentCellData].deserialize(from: jsonString) {
                cacheConfig.cacheDataToMemory(data as AnyObject, forKey: cacheListKey)
                completionHandler(datas as! [WechatMomentCellData])
            }
            return
        }
        
        let dataTask = URLSession.shared.dataTask(with: url) {(data, response, error) ->Void in
            if error != nil {
            }else{
                guard let jsonString = String(data: data!, encoding: String.Encoding.utf8) else {
                    return
                }
                if let datas = [WechatMomentCellData].deserialize(from: jsonString) {
                    cacheConfig.cacheDataToMemory(data as AnyObject, forKey: cacheListKey)
                    cacheConfig.cacheDataToDisk(data as AnyObject, forKey: cacheListKey)
                    
                    completionHandler(datas as! [WechatMomentCellData])
                }
            }
        }
        dataTask.resume()
    }
}
