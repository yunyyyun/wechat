//
//  ViewController.swift
//  WeChat
//
//  Created by mengyun on 2019/11/14.
//  Copyright © 2019 mengyun. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var sender: Sender?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        view.backgroundColor = .white
        self.title = "发现"
        
        let button = UIButton(frame: CGRect(x: 0, y: 100, width:self.view.bounds.width, height: 66))
        view.addSubview( button)
        button.backgroundColor = .red
        button.addTarget(self, action: #selector(goToWechatMomentView), for: .touchUpInside)
        
        let url: URL = URL(string: "https://thoughtworks-mobile-2018.herokuapp.com/user/jsmith")!
        let dataTask = URLSession.shared.dataTask(with: url) {(data, response, error) ->Void in
            if error != nil {
            }else{
                guard let jsonString = String(data: data!, encoding: String.Encoding.utf8) else {
                    return
                }
                if let data = Sender.deserialize(from: jsonString) {
                    self.sender = data
                }
            }
        }
        dataTask.resume()
    }
    
    @objc func goToWechatMomentView(){
        let vc = WechatMomentViewController()
        vc.sender = sender
        self.navigationController?.pushViewController(vc, animated: true)
    }

}

