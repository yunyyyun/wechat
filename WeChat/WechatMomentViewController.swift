//
//  WechatMomentViewController.swift
//  WeChat
//
//  Created by mengyun on 2019/11/17.
//  Copyright © 2019 mengyun. All rights reserved.
//

import UIKit
import MJRefresh

class WechatMomentViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{

    var sender: Sender?
    var list = [WechatMomentCellData]()
    var listTable = UITableView()
    // var refreshAction = UIRefreshControl()
    var headView = UIView()
    var backButton = UIButton()
    let headerHeight: CGFloat = 500
    
    var page = 1
    let header = MJRefreshNormalHeader()
    let footer = MJRefreshAutoNormalFooter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        let tab = UITableView(frame: CGRect(x: 0, y: 0, width:self.view.bounds.width, height: self.view.bounds.height - 0), style: .plain)
        let nib = UINib(nibName: "WechatMomentCell", bundle: nil)
        tab.register(nib, forCellReuseIdentifier: WechatMomentCell.reuseId())
        // tab.register(WechatMomentCell.self, forCellReuseIdentifier: WechatMomentCell.reuseId())
        tab.delegate = self
        tab.dataSource = self
        tab.separatorStyle = .none
        tab.estimatedRowHeight = 44
        tab.rowHeight = UITableView.automaticDimension
        view.addSubview(tab)
        listTable = tab
        
        self.setupHeadView()
        self.requestData()
        self.setupHeaderAndFooter()
        self.setupNavigation()
        
        // view.backgroundColor = .green
        // listTable.backgroundColor = .black
    }
    
    func setupNavigation() {
        self.title = "朋友圈"
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "back_arrow"), landscapeImagePhone: UIImage(named: "back_arrow"), style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPop))
        self.navigationController?.navigationBar.tintColor = .black
        
        let kNavBarHeight = self.navigationController!.navigationBar.frame.size.height
        let kStatusHeight = UIApplication.shared.statusBarFrame.size.height
        backButton.frame = CGRect(x: 11, y: CGFloat((kNavBarHeight-28)/2 + kStatusHeight), width: 28, height: 28)
        backButton.setImage(UIImage(named: "back_arrow"), for: UIControl.State.normal)
        backButton.addTarget(self, action: #selector(onPop), for: UIControl.Event.touchUpInside)
        view.addSubview(backButton)
    }
    
    // 导航栏设置
    func setupHeaderAndFooter() {
        // 下拉刷新
        header.setRefreshingTarget(self, refreshingAction: #selector(refreshDown))
        listTable.mj_header = header
        header.stateLabel.isHidden = true
        header.lastUpdatedTimeLabel.isHidden = true
        
        // 上拉加载
        footer.setRefreshingTarget(self, refreshingAction: #selector(loadMore))
        listTable.mj_footer = footer
    }
    // Header设置
    func setupHeadView() {
        // Header
        headView = UIView(frame: CGRect(x: 0, y: -headerHeight, width: self.view.bounds.width, height: headerHeight))
        listTable.contentInset = UIEdgeInsets(top: headerHeight-200, left: 0, bottom: 0, right: 0)
        listTable.addSubview(headView)
        
        let width = self.view.bounds.width
        headView.backgroundColor = .white
        
        let headImageView = UIImageView()
        headImageView.frame = CGRect(x: 0, y: 0, width: width, height: headerHeight-25)
        headImageView.backgroundColor = .lightGray
        headView.addSubview(headImageView)
        
        let avatarImageView = UIImageView()
        //avatarImageView.backgroundColor = .blue
        avatarImageView.layer.cornerRadius = 8
        avatarImageView.layer.masksToBounds = true
        avatarImageView.contentMode = .scaleToFill
        avatarImageView.backgroundColor = .gray
        avatarImageView.frame = CGRect(x: width-100, y: headerHeight-90, width: 80, height: 80)
        headView.addSubview(avatarImageView)
        
        let nickLabel = UILabel(frame: CGRect(x: 0, y: headerHeight-85, width: width-110, height: 40))
        // nickLabel.backgroundColor = .green
        nickLabel.textAlignment = .right
        nickLabel.textColor = .white
        nickLabel.font = UIFont.systemFont(ofSize: 20)
        headView.addSubview(nickLabel)
    
        if (sender==nil){
            Sender.getData { (sender) in
                DispatchQueue.main.async {
                    nickLabel.text = sender.nick
                    avatarImageView.setImageWithURL(imageUrl: sender.avatar, usePlaceholder: true)
                    headImageView.setImageWithURL(imageUrl: sender.profileImage, usePlaceholder: false)
                }
            }
        }
        else{
            nickLabel.text = sender?.nick
            avatarImageView.setImageWithURL(imageUrl: sender?.avatar, usePlaceholder: true)
            headImageView.setImageWithURL(imageUrl: sender?.profileImage, usePlaceholder: false)
        }
    }
    
    // 下拉刷新
    @objc func refreshDown() {
        page = 1
        self.requestData()
        print("refreshDown")
    }
    
    // 上拉
    @objc func loadMore() {
        if (page*5 < list.count){
            page = page+1
            listTable.reloadData()
            listTable.mj_footer.resetNoMoreData()
        }
        else{
            listTable.mj_footer.endRefreshingWithNoMoreData()
        }
        print("loadMore, page=", page)
    }
    
    // 请求表格数据
    func requestData(){
        WechatMomentCellData.getDatas { (datas) in
            self.handleListDatas(datas: datas )
        }
    }
    
    // 筛选数据，刷新列表
    func handleListDatas(datas: [WechatMomentCellData]) {
        self.list = datas.filter({ (data) -> Bool in
            var count = data.content.count
            count = count + (data.images != nil ? data.images!.count : 0)
            return count>0
        }) 
        DispatchQueue.main.async {
            self.listTable.mj_header.endRefreshing()
            self.listTable.reloadData()
            self.listTable.mj_footer.resetNoMoreData()
        }
    }
    
    // 代理方法
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let reuseID = WechatMomentCell.reuseId()
        var cell = tableView.dequeueReusableCell(withIdentifier: reuseID) as? WechatMomentCell
        if (cell == nil){
            cell = WechatMomentCell(style: .default, reuseIdentifier: reuseID)
        }
        cell?.data = list[indexPath.row]
        return cell!;
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return min(list.count, page*5)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = listTable.contentOffset.y;
        // print(offsetY)
        if (offsetY < -200){
            self.navigationController?.isNavigationBarHidden = true
        }
        else{
            self.navigationController?.isNavigationBarHidden = false
            self.navigationController?.navigationBar.subviews[0].alpha = (offsetY+200)/100
        }
    }
    
    @objc func onPop(){
        self.navigationController?.popViewController(animated: true)
    }

}
