//
//  CacheConfig.swift
//  WeChat
//
//  Created by mengyun on 2019/11/16.
//  Copyright © 2019 mengyun. All rights reserved.
//

import UIKit

class CacheConfig: NSObject {

    static let shared = CacheConfig()
    let ioQueue = dispatch_queue_serial_t(label: "com.image.cache")
    let memCache = NSCache<AnyObject, AnyObject>()
    let cachePath :String!
    
    private override init() {
        memCache.name = "com_image_cache"
        let paths = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true)
        cachePath = paths.first
        super.init()
    }
    
    // 写内存
    func cacheDataToMemory(_ data: AnyObject, forKey key: String?){
        memCache.setObject(data as AnyObject, forKey: key as AnyObject)
    }
    
    // 读内存
    func getDataFromMemory(forKey key: String?) -> AnyObject?{
       return memCache.object(forKey: key as AnyObject)
    }
    
    // 写文件缓存
    func cacheDataToDisk(_ data: AnyObject, forKey key: String?){
        if (key == nil){
            return
        }
        
        ioQueue.async {
            let filePath = self.cachePath + key!.zz_MD5
            _ = data.write(toFile: filePath, atomically: true)
        }
    }
    
    // 读文件缓存
    func getDataFromDisk(forKey key: String?) -> AnyObject?{
        if (key == nil){
            return nil
        }
        let filePath = self.cachePath + key!.zz_MD5
        if (FileManager.default.fileExists(atPath: filePath)){
            return NSMutableData(contentsOfFile: filePath)
        }
        return nil
    }
    
    
}
