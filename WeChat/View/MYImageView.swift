//
//  MYImageView.swift
//  WeChat
//  加载网络图片
//  todo： 图片太大了做简化
//  Created by mengyun on 2019/11/16.
//  Copyright © 2019 mengyun. All rights reserved.
//

import Foundation
import UIKit

extension UIImageView{
    
    // 通过标记来杜绝 因为复用导致图片错误的bug
    var taggedImageUrl: String? {
        get {
            return objc_getAssociatedObject(self, "taggedImageUrl") as? String
        }
        set {
            objc_setAssociatedObject(self, "taggedImageUrl", newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_COPY_NONATOMIC)
        }
    }
    
    func setImageWithURL(imageUrl : String?, usePlaceholder: Bool){
    
        guard imageUrl != nil else {
            return
        }
        
        taggedImageUrl = imageUrl?.zz_MD5
        let cacheConfig = CacheConfig.shared
        // 从缓存中取
        var data: Data? = cacheConfig.getDataFromMemory(forKey: imageUrl) as? Data
        if let goodData = data {
            let image = UIImage(data: goodData as Data)
            DispatchQueue.main.async {
                self.image = image
            }
            return
        }
        
        // 从磁盘取
        data = cacheConfig.getDataFromDisk(forKey: imageUrl) as? Data
        if let goodData = data {
            let image = UIImage(data: goodData as Data)
            DispatchQueue.main.async {
                self.image = image
            }
            cacheConfig.cacheDataToDisk(data as AnyObject, forKey: imageUrl)
            return
        }
        
        // 占位图
        if (usePlaceholder){
            DispatchQueue.main.async {
                self.image = UIImage(named: "placeholder")
            }
        }
        
        let charSet = CharacterSet.urlQueryAllowed as NSCharacterSet
        let mutSet = charSet.mutableCopy() as! NSMutableCharacterSet
        mutSet.addCharacters(in: "#")
        let encodingURL = imageUrl?.addingPercentEncoding(withAllowedCharacters: mutSet as CharacterSet)
        let url = URL(string: encodingURL!)!
        
        //创建请求对象
        let request = URLRequest(url: url)
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request, completionHandler: {
            (data, response, error) -> Void in
            if error != nil{
                print(error.debugDescription)
            }else{
                //将图片数据赋予UIImage
                let image = UIImage(data: data!)
                cacheConfig.cacheDataToMemory(data as AnyObject, forKey: imageUrl)
                cacheConfig.cacheDataToDisk(data as AnyObject, forKey: imageUrl)
                // 这里需要改UI，需要回到主线程
                DispatchQueue.main.async {
                    if (self.taggedImageUrl == imageUrl?.zz_MD5){
                        self.image = image
                    }
                    else{
                        print("复用导致图片加载错误，抛弃此次赋值")
                    }
                }
                
            }
        }) as URLSessionTask
        
        //使用resume方法启动任务
        dataTask.resume()
    }
}
