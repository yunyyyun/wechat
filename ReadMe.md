
### 仿微信朋友圈实现

按文件说明实现细节如下：

#### CacheConfig.swift
一个二级缓存（内存+磁盘）的简单实现，内存缓存使用 NSCache、磁盘缓存使用 FileManager写文件，使用一个串行队列来控制


#### MYImageView.swift
异步网络请求加载图片的一个实现， 结合CacheConfig来做缓存，使用关联对象taggedImageUrl 来防止 tableview 复用情况下图片设置错乱

#### WechatMomentCellData.swift
朋友圈需要用得到的model实现，用HandyJSON来做转换，主要作用是发送网络请求，将应答转换成需要的model，并结合CacheConfig来缓存请求的数据


#### WechatMomentCell.swift
朋友圈列表的cell，使用autoLayout约束，图片集使用collectionView，评论列表是一堆label

#### WechatMomentViewController.swift
朋友圈列表页面，主题是一个设置Header的tableview， 并且对navigationItem做了一些设置来达到微信朋友圈的效果，使用MJRefresh来做下拉刷新和上拉加载更多，并且做了伪翻页处理（因暂时没有网络翻页请求api）

#### FindViewController.swift
”发现“ 页面，通过此页面进入 “朋友圈” 页面